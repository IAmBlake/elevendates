//
//  DateChat.swift
//  ElevenDates
//
//  Created by Blake Steel on 12/1/15.
//  Copyright © 2015 Brett Keck. All rights reserved.
//

import UIKit

class DateChat: PFObject, PFSubclassing {
    class func parseClassName() -> String{
        return Constants.dateChat
    }
    
    @NSManaged var chatRoom : String
    @NSManaged var sender : DateUser
    @NSManaged var chatText : String
}
