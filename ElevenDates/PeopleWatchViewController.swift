//
//  PeopleWatchViewController.swift
//  ElevenDates
//
//  Created by Blake Steel on 11/10/15.
//  Copyright © 2015 Brett Keck. All rights reserved.
//

import UIKit

class PeopleWatchViewController: UIViewController, UIGestureRecognizerDelegate {
    @IBOutlet weak var previewUserCardView: UIView!
    @IBOutlet weak var previewUserImage: UIImageView!
    @IBOutlet weak var previewUserNameAge: UILabel!
    
    @IBOutlet weak var UserCardView: UIView!
    @IBOutlet weak var UserImage: UIImageView!
    @IBOutlet weak var UserNameAge: UILabel!
    
    @IBOutlet weak var passImage: UIImageView!
    @IBOutlet weak var failImage: UIImageView!
    
    private var dataStore = DataStore.sharedInstance
    private var cardStartCenter = CGPointZero
    private var potentialMatches: [DateUser]!
    private var skip = 0
    private var nextPreview = 0
    private var currentPotential: DateUser?
    private var nextPotential: DateUser?
    private var actionLock = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loading()
        
        UserCardView.layer.cornerRadius = 8.0
        UserCardView.clipsToBounds = true
        previewUserCardView.layer.cornerRadius = 8.0
        previewUserCardView.clipsToBounds = true
        
        let pan = UIPanGestureRecognizer(target: self, action: "moveUserCard:")
        pan.delegate = self
        self.UserCardView.addGestureRecognizer(pan)
        
        // Do any additional setup after loading the view.
        let heartButton = UIBarButtonItem(image: Images.Heart.image(), style: .Plain, target: self, action: "goToMatches:")
        let logoutButton = UIBarButtonItem(image: Images.Logout.image(), style: .Plain, target: self, action: "logout:")
        navigationItem.rightBarButtonItems = [logoutButton, heartButton]
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        cardStartCenter = CGPoint(x: self.view.frame.size.width / 2.0, y: UserCardView.center.y)
        loadCards(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
        // MARK: - private functions
    private func loading() {
        UserImage.image = nil
        UserNameAge.text = ""
        previewUserImage.image = nil
        previewUserNameAge.text = ""
    }
    private func loadCards(reset : Bool) {
        if reset {
            loading()
            skip = 0
        }
        dataStore.getPotentials(skip) { (potentialReturn) -> () in
            if let potentials = potentialReturn {
                self.skip += potentials.count
                self.potentialMatches = potentials
                self.nextPreview = 0
                if reset {
                    self.firstLoad()
                }
            }
        }
    }
    private func firstLoad(){
        if potentialMatches.isEmpty {
            return
        }
        
        currentPotential = potentialMatches.first
        if let currentUser = currentPotential {
            UserNameAge.text = currentUser.displayText
            currentUser.image.getDataInBackgroundWithBlock({ (data, error) -> Void in
                if error == nil && data != nil {
                    self.UserImage.image = UIImage(data: data!)
                }
            })
        }
        nextPreview++
        loadPreview()
    }
    
    private func loadPreview() {
        if nextPreview >= potentialMatches.count {
            nextPotential = nil
            previewUserImage.image = Images.Closed.image()
            return
        }
        nextPotential = potentialMatches[nextPreview]
        
        if let previewUser = nextPotential {
            previewUserNameAge.text = previewUser.displayText
            previewUser.image.getDataInBackgroundWithBlock({ (data, error) -> Void in
                self.previewUserImage.image = UIImage(data: data!)
            })
        }
        if ++nextPreview >= potentialMatches.count {
            loadCards(false)
        }
    }
    
    private func nextUserCard(){
        currentPotential = nextPotential
        UserImage.image = previewUserImage.image
        UserNameAge.text = previewUserNameAge.text
        
        UserCardView.center = cardStartCenter
        failImage.alpha = 0
        passImage.alpha = 0
        
        previewUserImage.image = nil
        previewUserNameAge.text = ""
        
        loadPreview()
        
    }
    
    private func doNo() {
        //Create a function for if we're not interested.
        //send the picture away if user says no
        if currentPotential == nil || actionLock {
            return
        }
        actionLock = true
        dataStore.nopePerson(currentPotential!)
        failImage.alpha = 1
        
        let offLeft = CGPoint(x: -(self.view.frame.size.width + self.UserCardView.frame.size.width), y: 0)
        UIView.animateWithDuration(0.6, animations: { () -> Void in
            self.UserCardView.center = offLeft
            }) { (finished) -> Void in
                if finished {
                    self.nextUserCard()
                    self.actionLock = false
            }
        }
        
    }
    private func doYes() {
        //Create a function for if we're not interested.
        //send the picture away if user says no
        if currentPotential == nil || actionLock {
            return
        }
        actionLock = true
        dataStore.likePerson(currentPotential!)
        passImage.alpha = 1
        
        let offRight = CGPoint(x: (self.view.frame.size.width + self.UserCardView.frame.size.width), y: 0)
        UIView.animateWithDuration(0.6, animations: { () -> Void in
            self.UserCardView.center = offRight
            }) { (finished) -> Void in
                if finished {
                    self.nextUserCard()
                    self.actionLock = false
                }
        }
        
    }
    
        // MARK: -IBACTIONS
    
    @IBAction func logout(sender : UIBarButtonItem) {
        PFUser.logOut()
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.loginCheck()
    }
    
    @IBAction func goToMatches(sender: UIBarButtonItem){
        
    }
    
    @IBAction func noTapped(sender: UIButton){
        doNo()
    }
    @IBAction func yesTapped(sender: UIButton){
        doYes()
    }
    @IBAction func infoTapped(sender: UIButton){
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func moveUserCard(sender : UIGestureRecognizer) {
        func between <T : Comparable> (item : T, minItem : T, maxItem : T) -> T {
            if item > minItem {
                return min(maxItem, item)
            }
            else {
                return max(minItem, item)
            }
        }
        if currentPotential == nil {
            return
        }
        let panRecognizer = sender as! UIPanGestureRecognizer
        let currentPoint = UserCardView.center
        let translation = panRecognizer.translationInView(UserCardView.superview!)
        let delta = currentPoint.x - cardStartCenter.x
        let alpha = between(abs(delta), minItem: 0, maxItem: 75.0) / 75.0
        
        switch panRecognizer.state {
        case .Changed:
            UserCardView.center = CGPoint(x: currentPoint.x + translation.x, y: currentPoint.x + translation.x)
            if delta > 0 {
                failImage.alpha = 0
                passImage.alpha = alpha
            }
            else {
                failImage.alpha = alpha
                passImage.alpha = 0
            }
            panRecognizer.setTranslation(CGPointZero, inView: UserCardView.superview!)
        case .Ended:
            if alpha < 1.0 {
                UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.8, options: [], animations: { () -> Void in
                    self.UserCardView.center = self.cardStartCenter
                    self.failImage.alpha = 0
                    self.passImage.alpha = 0
                    }, completion: nil)
            }
            else {
                if delta > 0 {
                    doYes()
                }
                else {
                    doNo()
                }
            }
        default:
            break
        }
    }

}
