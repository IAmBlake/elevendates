//
//  ProfileViewController.swift
//  ElevenDates
//
//  Created by Brett Keck on 10/22/15.
//  Copyright © 2015 Brett Keck. All rights reserved.
//

import UIKit

class ProfileViewController: UITableViewController {
    
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var genderSegment: UISegmentedControl!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var discoverableSwitch: UISwitch!
    @IBOutlet weak var ageSlider: UISlider!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var showText: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // round off the corners of the image
        
        
        // make a call to update the UI
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Save the data
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateUI() {
        // download their avatar
        
        
        
        // show their name
        
        
        // show their gender
        
        
        // set the switch to their discoverable value
        
        
        // show their age (label and slider)
        
        
        // show their preference
        
        
    }
    
    
    // MARK: - IBActions
    // discoverable changed
    
    
    
    // age changed
    
    
    
    // gender changed
    
    
    @IBAction func showMenOnly(segue:UIStoryboardSegue) {
        // change gender pref and update the UI
        
    }
    
    @IBAction func showWomenOnly(segue:UIStoryboardSegue) {
        // change gender pref and update the UI
        
    }
    
    @IBAction func showBoth(segue:UIStoryboardSegue) {
        // change gender pref and update the UI
        
    }
}









